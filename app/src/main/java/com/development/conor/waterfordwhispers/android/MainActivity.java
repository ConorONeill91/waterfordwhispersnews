package com.development.conor.waterfordwhispers.android;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.development.conor.waterfordwhispers.R;
import com.development.conor.waterfordwhispers.java.ScrapeObject;
import com.development.conor.waterfordwhispers.java.Scraper;
import com.twotoasters.jazzylistview.JazzyHelper;
import com.twotoasters.jazzylistview.JazzyListView;


import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private JazzyListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Waterford Whispers News");
        list = (JazzyListView)findViewById(R.id.jazzy_list);
        list.setTransitionEffect(JazzyHelper.ZIPPER);
        new ScrapeBreaking().execute();
        new ScrapeOthers().execute();




//          // Needs to be parcelable
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
//                ScrapeObject clickedResult = (ScrapeObject) list.getItemAtPosition(position);
//                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                intent.putExtra("ArticleIntent", clickedResult);
//                startActivity(intent);
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ScrapeBreaking extends AsyncTask<Void, Void, ArrayList<ScrapeObject>> {

        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            Log.d("WW","Pre Execute");
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this, "One Second..",
                    "Serving up your latest Waterford Whispers News..");
        }


        @Override
        protected ArrayList<ScrapeObject> doInBackground(Void... params) {
            Log.d("WW","BackGround");
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            ArrayList<ScrapeObject> results = new ArrayList<>();
            Scraper scraper = new Scraper();
            scraper.scrapeBreaking(results);
            return results;
        }
        // Pass scraped data to adapter.
        @Override
        protected void onPostExecute(ArrayList<ScrapeObject> results) {
            Log.d("WW","Post Execute");
            progressDialog.cancel();
            MyListAdapter adapter = new MyListAdapter(MainActivity.this,R.layout.item_layout,results);
            adapter.notifyDataSetChanged();
            list.setAdapter(adapter);
        }

    }

    private class ScrapeOthers extends AsyncTask<Void, Void, ArrayList<ScrapeObject>> {

        private ProgressDialog progressDialog;


        @Override
        protected void onPreExecute() {
            Log.d("WW", "Pre Execute Others");
            super.onPreExecute();

        }


        @Override
        protected ArrayList<ScrapeObject> doInBackground(Void... params) {
            Log.d("WW","BackGround Others");
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            ArrayList<ScrapeObject> results = new ArrayList<>();
            Scraper scraper = new Scraper();
            scraper.scrapeRecents(results);
            return results;
        }
        // Pass scraped data to adapter.
        @Override
        protected void onPostExecute(ArrayList<ScrapeObject> results) {
            Log.d("WW", "Post Execute Others");

            MyListAdapter adapter = new MyListAdapter(MainActivity.this,R.layout.item_layout,results);
            adapter.notifyDataSetChanged();
            list.setAdapter(adapter);
        }

    }
    // Back confirmation
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }


}
