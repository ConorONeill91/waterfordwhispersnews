package com.development.conor.waterfordwhispers.java;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by conor on 14/05/15.
 */
public class Scraper {
    // TODO ---> Refactor!  In bits like your aul wan

        private static Logger logger = Logger.getLogger(Scraper.class.getName());

    public void scrapeBreaking(ArrayList<ScrapeObject> scrapeObjects){
        try {
            Document doc = Jsoup.connect("http://waterfordwhispersnews.com/")
                    .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36").get();
            Elements results = doc.getElementsByClass("other-news");
            boolean imageless;
            for (Element element : results) {
                imageless = false;
                String content = "";
                ScrapeObject result = new ScrapeObject();
                result.setHref(element.select("a").first().attr("href"));
                // TODO : Handle Viral Photo & Poll Articles - Discarding for now
                if(element.select("h3").text().toLowerCase().contains("viral") || element.select("h3").text().toLowerCase().contains("photos") || element.select("h3").text().toLowerCase().contains("poll")){
                    continue;
                }
                result.setTitle(element.select("h3").text());
                Document contentDoc = Jsoup.connect(result.getHref())
                        .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36").get(); // 403 Forbidden
                Elements contentResults = contentDoc.select("div.entry");
                for(Element contentResult : contentResults) {
                    content += contentResult.select("p").text(); // String Builder!
                    try {
                        result.setImageUrl(contentResult.select("img").first().attr("src"));
                    }
                    catch(NullPointerException e){ // Discards no photo articles
                        imageless = true;
                        break;
                    }
                }
                if(imageless){ // Discard if imageless
                    continue;
                }
                result.setContent(content);
                scrapeObjects.add(result);
                // TODO remove
                for(ScrapeObject s : scrapeObjects){
                    logger.info(s.getContent() + "\n" + s.getHref() + "\n" + s.getTitle() + "\n" + s.getImageUrl());
                }

            }

        }
        catch(IOException e){
            logger.info("Error connecting to WW");
        }
    }



    public void scrapeRecents(ArrayList<ScrapeObject> scrapeObjects){
        try {
            Document doc = Jsoup.connect("http://waterfordwhispersnews.com/")
                    .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36").get();
            Elements results = doc.getElementsByClass("recent-item");
            boolean imageless;
            for (Element element : results) {
                imageless = false;
                String content = "";
                ScrapeObject result = new ScrapeObject();
                result.setHref(element.select("a").first().attr("href"));
                // TODO : Handle Viral Photo & Poll Articles - Discarding for now
                if(element.select("h3").text().toLowerCase().contains("viral") || element.select("h3").text().toLowerCase().contains("photos") || element.select("h3").text().toLowerCase().contains("poll")){
                    continue;
                }
                result.setTitle(element.select("h3").text());
                Document contentDoc = Jsoup.connect(result.getHref())
                        .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36").get(); // 403 Forbidden
                Elements contentResults = contentDoc.select("div.entry");
                for(Element contentResult : contentResults) {
                    content += contentResult.select("p").text(); // String Builder!
                   try {
                       result.setImageUrl(contentResult.select("img").first().attr("src"));
                   }
                   catch(NullPointerException e){ // Discards no photo articles
                       imageless = true;
                       break;
                   }
                }
                if(imageless){ // Discard if imageless
                    continue;
                }
                result.setContent(content);
                scrapeObjects.add(result);
                // TODO remove
                for(ScrapeObject s : scrapeObjects){
                    logger.info(s.getContent() + "\n" + s.getHref() + "\n" + s.getTitle() + "\n" + s.getImageUrl());
                }

            }

        }
        catch(IOException e){
            logger.info("Error connecting to WW");
        }

    }
//    // TODO : Remove after debugging
    public static void main(String [] args){
        Scraper s = new Scraper();
        ArrayList<ScrapeObject> results = new ArrayList<>();
        s.scrapeBreaking(results);
    }
}

