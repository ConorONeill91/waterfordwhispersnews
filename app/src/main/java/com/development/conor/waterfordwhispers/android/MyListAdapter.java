package com.development.conor.waterfordwhispers.android;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.development.conor.waterfordwhispers.R;
import com.development.conor.waterfordwhispers.java.ScrapeObject;
import com.development.conor.waterfordwhispers.java.Scraper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by coneill on 20/05/2015.
 */

public class MyListAdapter extends ArrayAdapter<ScrapeObject> {
    private ArrayList<ScrapeObject> items;

    public MyListAdapter(Context context, int resId, ArrayList<ScrapeObject> items){
        super(context, resId);
        this.items = items;
    }
    @Override
    public int getCount(){
      return items.size();
    };

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View w = convertView;
        Log.d("WW","" + position);

        if(w == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            w = inflater.inflate(R.layout.item_layout,parent,false);
        }

        ScrapeObject scrapeObject = items.get(position);


        TextView title = (TextView)w.findViewById(R.id.title);
        TextView content = (TextView)w.findViewById(R.id.content); // TODO : Make scrollable.. Maybe not TV
        ImageView image = (ImageView)w.findViewById(R.id.image);
        //Log.i("WW", "" + scrapeObject.getContent() + scrapeObject.getHref() + scrapeObject.getTitle() + scrapeObject.getImageUrl());

        // Check  content is not null
        if(scrapeObject.getImageUrl() != null && scrapeObject.getTitle() != null && scrapeObject.getContent() != null) {
            Picasso.with(getContext()).load(scrapeObject.getImageUrl()).resize(275, 275).into(image);
            title.setText(scrapeObject.getTitle());
            content.setText(scrapeObject.getContent());
        }

//           // Picasso.with(getContext()).load(scrapeObject.getImageUrl()).resize(275, 275).into(image);
//            title.setText("Conor");
//            content.setText("O'neill");


        return w;
    }


}

