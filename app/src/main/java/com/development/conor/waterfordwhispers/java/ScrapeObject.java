package com.development.conor.waterfordwhispers.java;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by conor on 14/05/15.
 */
public class ScrapeObject {
    private String href;
    private String title;
    private String content;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ScrapeObject(){}

//    // Parcelling
//    public ScrapeObject(Parcel in) {
//        String [] data = new String[4];
//        in.readStringArray(data);
//        this.href = data[0];
//        this.title = data[1];
//        this.content = data[2];
//        this.imageUrl = data[3];
//    }
//
//
//    public int describeContents() {
//        return 0;
//    }
//
//
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeStringArray(new String[] {this.href, this.title,this.content,this.imageUrl});
//    }
//
//    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
//        public ScrapeObject createFromParcel(Parcel in) {
//            return new ScrapeObject(in);
//        }
//
//        public ScrapeObject[] newArray(int size) {
//            return new ScrapeObject[size];
//        }
//
//    };

}
