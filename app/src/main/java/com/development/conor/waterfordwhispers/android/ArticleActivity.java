package com.development.conor.waterfordwhispers.android;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.development.conor.waterfordwhispers.R;
import com.development.conor.waterfordwhispers.java.ScrapeObject;
import com.squareup.picasso.Picasso;

public class ArticleActivity extends Activity {

    private ImageView article_image;
    private TextView article_title;
    private TextView article_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        setTitle("Whispers from Waterford");

        ScrapeObject clickedResult = (ScrapeObject)getIntent().getParcelableExtra("ClickedResult");

        article_image = (ImageView)findViewById(R.id.article_image);
        article_title = (TextView)findViewById(R.id.article_title);
        article_content = (TextView)findViewById(R.id.article_content);
        article_content.setMovementMethod(new ScrollingMovementMethod());

        article_title.setText(clickedResult.getTitle());
        article_content.setText(clickedResult.getContent());
        Picasso.with(getApplicationContext()).load(clickedResult.getImageUrl()).resize(1000, 500).into(article_image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_article, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
